package matiarmanush.com.matiarapp.utils.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import matiarmanush.com.matiarapp.utils.Utils;


public class NetworkReceiver extends BroadcastReceiver {
    private NetworkListener networkListener;
    Context context;

    public NetworkReceiver(NetworkListener networkListener) {
        this.networkListener = networkListener;
    }
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if (Utils.isOnline(context)) {
            this.networkListener.onNetworkAvailable();
        } else {
            this.networkListener.onNetworkUnavailable();
        }

    }

    public interface NetworkListener {
        void onNetworkAvailable();

        void onNetworkUnavailable();
    }





}
