package matiarmanush.com.matiarapp.utils.enums;

/**
 * Created by Gurvinder rajpal on 01-3-17.
 */

public enum  ImageType {
    BACK("back"),FRONT("front"),SIDE("side");

    public final String type;

    ImageType(String type) {

        this.type = type;
    }
}
