package matiarmanush.com.matiarapp.Models;

import matiarmanush.com.matiarapp.generic.ViewModel;

public class ProductModel implements ViewModel {
String CartItem,ProductId,CategoryId,ParentId,Name,Image,Quantity, Count,isAdded,price,newPrice;
    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getCartItem() {
        return CartItem;
    }

    public void setCartItem(String cartItem) {
        CartItem = cartItem;
    }

    public String getProductId() {
        return ProductId;
    }

    public void setProductId(String productId) {
        ProductId = productId;
    }

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String parentId) {
        ParentId = parentId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public String getIsAdded() {
        return isAdded;
    }

    public void setIsAdded(String isAdded) {
        this.isAdded = isAdded;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public int layoutId() {
//        return R.layout.rowitem_parlour;
        return 0;
    }

    @Override
    public String emptyTitle() {
        return null;
    }

}