package matiarmanush.com.matiarapp.generic;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import matiarmanush.com.matiarapp.R;
import matiarmanush.com.matiarapp.utils.User;

public class HandleToolbar {
    private Activity activity;
    private TextView toolbarTitle;
    ImageView tool_back, tool_cart, tool_nav, tool_cross, tool_search, tool_notification;
    User user;
    //    ImageLoader imageLoader;
    OnCrossClickListener onPopupClickListener;
    private OnBackButtonClickListener startChatClickListener;
    OnTextClickListener onTextClickListener;
    OnCartClickListener onCartClickListener;
    OnSearchClickListener onSearchClickListener;
    OnNotificationClickListener onNotificationClickListener;
    OnNavigationClickListener onNavigationClickListener;

    public HandleToolbar(Activity activity) {
        this.activity = activity;
        user = User.getInstance();
//        imageLoader = ImageLoader.getInstance();
        initToolbar();
    }

    private void initToolbar() {
        toolbarTitle = (TextView) activity.findViewById(R.id.tool_text);
        tool_back = (ImageView) activity.findViewById(R.id.tool_back);
        tool_nav = (ImageView) activity.findViewById(R.id.tool_navigation);
        tool_cross = (ImageView) activity.findViewById(R.id.tool_cross);
        tool_notification = (ImageView) activity.findViewById(R.id.tool_notification);
        tool_search = (ImageView) activity.findViewById(R.id.tool_search);
        tool_cart = (ImageView) activity.findViewById(R.id.tool_cart);

        tool_back.setVisibility(View.GONE);
        tool_nav.setVisibility(View.GONE);
        tool_notification.setVisibility(View.GONE);
        tool_back.setVisibility(View.GONE);
        tool_search.setVisibility(View.GONE);
        tool_cart.setVisibility(View.GONE);
        toolbarTitle.setVisibility(View.INVISIBLE);

        handleClicks();
    }


    private void handleClicks() {
        tool_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.finish();
            }
        });
        tool_nav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNavigationClickListener != null)
                    onNavigationClickListener.onClick();
            }
        });
        tool_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (startChatClickListener != null)
                    startChatClickListener.onClick();
            }
        });
        tool_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onSearchClickListener != null)
                    onSearchClickListener.onClick();
            }
        });
        tool_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onCartClickListener != null)
                    onCartClickListener.onClick();
            }
        });
        tool_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onNotificationClickListener != null)
                    onNotificationClickListener.onClick();
            }
        });
        toolbarTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onTextClickListener != null)
                    onTextClickListener.onClick();
            }
        });

    }

    public void showCenterText() {
        toolbarTitle.setVisibility(View.VISIBLE);
    }

    public void showCrossIcon() {
        tool_cross.setVisibility(View.VISIBLE);
    }

    public void showNAvIcon() {
        tool_nav.setVisibility(View.VISIBLE);
    }

    public void showBackIcon() {
        tool_back.setVisibility(View.VISIBLE);
    }

    public void showCartIcon() {
        tool_cart.setVisibility(View.VISIBLE);
    }

    public void showNotificationIcon() {
        tool_notification.setVisibility(View.VISIBLE);
    }

    public void showSearchIcon() {
        tool_search.setVisibility(View.VISIBLE);
    }

    public void setText(String s) {
        toolbarTitle.setText(s);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            toolbarTitle.setText(Html.fromHtml(s, Html.FROM_HTML_OPTION_USE_CSS_COLORS));
//        } else {
//        }

    }
    public interface OnNavigationClickListener {
        void onClick();
    }

    public interface OnBackButtonClickListener {
        void onClick();
    }

    public interface OnCrossClickListener {
        void onClick(int itemId);
    }

    public interface OnTextClickListener {
        void onClick();
    }

    public interface OnNotificationClickListener {
        void onClick();
    }

    public interface OnCartClickListener {
        void onClick();
    }

    public interface OnSearchClickListener {
        void onClick();
    }

    public void setOnTextClickListener(OnTextClickListener onTextClickListener) {
        this.onTextClickListener = onTextClickListener;
    }

    public void setOnNotificationClickListener(OnNotificationClickListener onNotificationClickListener) {
        this.onNotificationClickListener = onNotificationClickListener;
    }

    public void setOnCartClickListener(OnCartClickListener onCartClickListener) {
        this.onCartClickListener = onCartClickListener;
    }

    public void setOnSearchClickListener(OnSearchClickListener onSearchClickListener) {
        this.onSearchClickListener = onSearchClickListener;
    }

    public void setOnNavigationClickListener(OnNavigationClickListener onNavigationClickListener) {
        this.onNavigationClickListener = onNavigationClickListener;
    }

    public void setOnBackButtonClickListener(OnBackButtonClickListener startChatClickListener) {

        this.startChatClickListener = startChatClickListener;
    }

    public void setOnCrossClickListener(OnCrossClickListener onPopupClickListener) {
        this.onPopupClickListener = onPopupClickListener;
    }
}
