package matiarmanush.com.matiarapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import matiarmanush.com.matiarapp.utils.Utils;


public class DB_queries {
    private final Context mContext;
    private final DatabaseHelper databaseHelper;
    private SQLiteDatabase sqlite;
    Utils utils;

    public DB_queries(Context mContext) {
        this.mContext = mContext;
        // TODO Auto-generated constructor stub
         databaseHelper=new DatabaseHelper(mContext);
        sqlite=databaseHelper.getWritableDatabase();
        utils = new Utils();
    }

    public boolean insertCartItem(String ProductId, String CategoryId, String ParentId, String Name, String Image,String Count,String isAdded,String price ) {
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.Column.Cart.ProductId, ProductId);
        values.put(DatabaseHelper.Column.Cart.CategoryId, CategoryId);
        values.put(DatabaseHelper.Column.Cart.ParentId, ParentId);
        values.put(DatabaseHelper.Column.Cart.Name, Name);
        values.put(DatabaseHelper.Column.Cart.Image, Image);
        values.put(DatabaseHelper.Column.Cart.Count, Count);
        values.put(DatabaseHelper.Column.Cart.isAdded, isAdded);
        values.put(DatabaseHelper.Column.Cart.price, price);

        long id = 0;
        try {

                    id = sqlite.insert(DatabaseHelper.Table.CartItem, null, values);

        } catch (Exception e) {
            Log.d("--sql error", "while insert in database: " + e);
            e.printStackTrace();
            return false;
        }
        if (id > 0) {
            return true;
        }

        return false;

    }

//    public boolean updateTripIDwitActualID(int actualid, int oldID) {
//        ContentValues values = new ContentValues();
//
//        values.put(Database.Filter_Items.TRIP_Trip_ID, String.valueOf(actualid));
//
//        long id;
//        try {
//
//            id = sqlite.update(Database.Filter_Items.Table_TRIP_Details, values, Database.Filter_Items.TRIP_Trip_ID + "=?", new String[]{String.valueOf(oldID)});
//
//        } catch (Exception e) {
//            Log.d("--sql error", "while insert in database: " + e);
//            e.printStackTrace();
//            return false;
//        }
//        if (id > 0) {
//            return true;
//        }
//
//        return false;
//
//    }

//    public boolean updateTRIPDETAILS_NOTIFY(String nfckey, String status, String signstatus, String presentstatusoff) {
//        ContentValues values = new ContentValues();
//
//        values.put(Database.Filter_Items.TRIP_Student_STATUS, status);
//        values.put(Database.Filter_Items.TRIP_Student_SIGN_STATUS, signstatus);
//        values.put(Database.Filter_Items.TRIP_Student_Sign_PresentStatus, presentstatusoff);
//
//        long id;
//        try {
//
//            id = sqlite.update(Database.Filter_Items.Table_TRIP_Details, values, Database.Filter_Items.TRIP_Student_NFC + "=?", new String[]{nfckey});
//
//        } catch (Exception e) {
//            Log.d("--sql error", "while insert in database: " + e);
//            e.printStackTrace();
//            return false;
//        }
//        if (id > 0) {
//            return true;
//        }
//
//        return false;
//
//    }

//    public boolean insertQTNFC(String out, String name) {
//        ContentValues values = new ContentValues();
//
//        values.put(Database.Filter_Items.QTNFC_ID, out);
//        values.put(Database.Filter_Items.QTNFC_NAME, name);
//
//        long id;
//        try {
//            id = sqlite.insert(Database.Filter_Items.QT_NEW_NFCDATA, null, values);
//        } catch (Exception e) {
//            Log.d("--sql error", "while insert in database: " + e);
//            e.printStackTrace();
//            return false;
//        }
//        if (id > 0) {
//            return true;
//        }
//
//        return false;
//
//    }


//    public boolean insertScannedCard_Teacher(String key, String response) {
//        ContentValues values = new ContentValues();
//        values.put(Database.Filter_Items.Scan_response_Key, key);
//        values.put(Database.Filter_Items.Scan_response_text, response);
//
//        long id = 0;
//        try {
//            if (checkScannedcardList(key)) {
//                sqlite.update(Database.Filter_Items.ScannedCard_Detail, values, Database.Filter_Items.Scan_response_Key + "=?", new String[]{key});
//            } else {
//                id = sqlite.insert(Database.Filter_Items.ScannedCard_Detail, null, values);
//            }
//        } catch (Exception e) {
//            Log.d("--sql error", "while insert in database: " + e);
//            e.printStackTrace();
//            return false;
//        }
//        if (id > 0) {
//            return true;
//        }

//        return false;
//
//    }


//    public boolean insertNewTripDetails_QT(String key, String response) {
//        ContentValues values = new ContentValues();
//        values.put(Database.Filter_Items.Q_response_Key, key);
//        values.put(Database.Filter_Items.Q_response_text, response);
//
//        long id = 0;
//        try {
//            if (checkQuickcardList(key)) {
//                id = sqlite.update(Database.Filter_Items.Quicktrip_Detail, values, Database.Filter_Items.Q_response_Key + "=?", new String[]{key});
//            } else {
//                id = sqlite.insert(Database.Filter_Items.Quicktrip_Detail, null, values);
//            }
//        } catch (Exception e) {
//            Log.d("--sql error", "while insert in database: " + e);
//            e.printStackTrace();
//            return false;
//        }
//        if (id > 0) {
//            return true;
//        }
//
//        return false;
//
//    }

//    public ArrayList<Bean> getTripDetailsFrom_DB(int tripid) {
//        // TODO Auto-generated method stub
//        String name = null, nfc, status, signstatus, sign_id, stripid;
//        ArrayList<Bean> list = new ArrayList<>();
//        try {
//            Cursor c = sqlite.query(Database.Filter_Items.Table_TRIP_Details, new String[]{Database.Filter_Items.TRIP_Student_NAME, Database.Filter_Items.TRIP_Student_NFC, Database.Filter_Items.TRIP_Student_Sign_PresentStatus, Database.Filter_Items.TRIP_Student_STATUS, Database.Filter_Items.TRIP_Student_SIGN_STATUS, Database.Filter_Items.TRIP_Trip_ID}, Database.Filter_Items.TRIP_Trip_ID + "=?", new String[]{String.valueOf(tripid)}, null, null, null);
//
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//                name = c.getString(c.getColumnIndex(Database.Filter_Items.TRIP_Student_NAME));
//                nfc = c.getString(c.getColumnIndex(Database.Filter_Items.TRIP_Student_NFC));
//                sign_id = c.getString(c.getColumnIndex(Database.Filter_Items.TRIP_Student_Sign_PresentStatus));
//                status = c.getString(c.getColumnIndex(Database.Filter_Items.TRIP_Student_STATUS));
//                signstatus = c.getString(c.getColumnIndex(Database.Filter_Items.TRIP_Student_SIGN_STATUS));
//                Bean bean = new Bean();
//                bean.setTrip_Sname(name);
//                bean.setTripSNFC(nfc);
//                bean.setTrip_signId(sign_id);
//                bean.setTrup_Status(status);
//                bean.setTrip_signStatus(signstatus);
//                //  need to add status
//                list.add(bean);
//
//                Log.d("tripdetails", "name " + name + " tripid " + tripid + " nfc " + nfc);
//                c.moveToNext();
//            }
//            c.close();
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//            e.printStackTrace();
//        }
//        return list;
//    }

//    public String getScannedcardList(String key) {
//        // TODO Auto-generated method stub
//        String response = null;
//        try {
//            Cursor c = sqlite.query(Database.Filter_Items.ScannedCard_Detail, new String[]{Database.Filter_Items.Scan_response_text}, Database.Filter_Items.Scan_response_Key + "=?", new String[]{key}, null, null, null);
//
//            if (c.moveToFirst()) {
//                response = c.getString(c.getColumnIndex(Database.Filter_Items.Scan_response_text));
//            }
//            c.close();
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//            e.printStackTrace();
//        }
//        return response;
//    }
//
//    public String getdetailsWithKEY_QT(String key) {
//        // TODO Auto-generated method stub
//        String response = null;
//        try {
//            Cursor c = sqlite.query(Database.Filter_Items.Quicktrip_Detail, new String[]{Database.Filter_Items.Q_response_text}, Database.Filter_Items.Q_response_Key + "=?", new String[]{key}, null, null, null);
//
//            if (c.moveToFirst()) {
//                response = c.getString(c.getColumnIndex(Database.Filter_Items.Q_response_text));
//            }
//            c.close();
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//            e.printStackTrace();
//        }
//        return response;
//    }

//    public int deletetable(String table) {
//        // TODO Auto-generated method stub
//        int c = -1;
//        try {
//            c = sqlite.delete(table, null, null);
//
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//            e.printStackTrace();
//        }
//        Log.d("--cccccccccccccccccc", "while fetching: " + c);
//
//        return c;
//    }





    public String getPresentStatus(String status, String signstatus) {
        if (signstatus.equalsIgnoreCase("0")) {
            return "notstarted";

        } else if (signstatus.equalsIgnoreCase("1")) {
            // 1 present , 0 absent
            if (status.equalsIgnoreCase("1")) {
                return "present";
            } else {
                return "absent";
            }
        } else if (signstatus.equalsIgnoreCase("2")) {
            if (status.equalsIgnoreCase("1")) {
                return "absent";
            } else if (status.equalsIgnoreCase("2")) {
                return "present";
            }
            // 1 absent, 2 present
        }
        return status;
    }

//    public JSONArray getAllScannedCard() {
//        String myResponse = getScannedcardList(Constants.ScanDetail);
//        Log.d(" filter  getfilter", "" + myResponse);
//        JSONArray myArray = new JSONArray();
//        try {
//            if (myResponse != null)
//                myArray = new JSONArray(myResponse);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return myArray;
//    }



//    public boolean checkTripIDExists(String id) {
//        // TODO Auto-generated method stub
//        try {
//            Cursor c = sqlite.query(Database.Filter_Items.Table_TRIP_Details, new String[]{Database.Filter_Items.TRIP_Trip_ID}, Database.Filter_Items.TRIP_Trip_ID + "=?", new String[]{id}, null, null, null);
//
//            if (c.moveToFirst()) {
//                c.close();
//                return true;
//            }
//            c.close();
//            return false;
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//            e.printStackTrace();
//        }
//        return false;
//    }

    public void addFormDetails_QT(int i1, int i, String tripname, String tripdescription, String address, double lat, double lng, String startdate, String enddate, int aClass, String tid) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("trip_id", i1);
            jsonObject.put("actual_id", i);
            jsonObject.put("trip_title", tripname);
            jsonObject.put("trip_description", tripdescription);
            jsonObject.put("start_date", startdate);
            jsonObject.put("end_date", enddate);
            jsonObject.put("address", address);
            jsonObject.put("lat", lat);
            jsonObject.put("lng", lng);
            jsonObject.put("class_id", aClass);
            jsonObject.put("teacher_id", tid);
//            boolean status = insertNewTripDetails_QT(Constants.QuickScanDetail, jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


//    public boolean isExist(String productID) {
////        // TODO Auto-generated method stub
//        try {
//            Cursor c = sqlite.query(Database.CART_orders.ScannedCard_Detail,
//                    new String[]Database.CART_orders.Order_ProductID},
//                    Database.CART_orders.Order_ProductID + "=?", new String[]{productID},
//                    null, null, null);
//            if (c.moveToFirst()) {
//
//
//                return true;
//            }
//            c.close();
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//            e.printStackTrace();
//        }
//        return false;
//    }

//    public ArrayList<Bean> get_orders() {
//        // TODO Auto-generated method stub
//        ArrayList<Bean> list = new ArrayList<Bean>();
//        try {
//            Cursor c = sqlite.query(Database.CART_orders.ScannedCard_Detail, new String[]{Database.CART_orders.Order_ProductID,Database.CART_orders.Order_image, Database.CART_orders.Order_name, Database.CART_orders.Order_prize,Database.CART_orders.Order_quanity, Database.CART_orders.Order_sellername, Database.CART_orders.Order_qCount,Database.CART_orders.ID ,Database.CART_orders.Order_AuthorID}, null, null, null, null, null);
//
//
//            c.moveToFirst();
//            while (!c.isAfterLast()) {
//                String product_id = c.getString(c.getColumnIndex(Database.CART_orders.Order_ProductID));
//                String name = c.getString(c.getColumnIndex(Database.CART_orders.Order_name));
//                String image = c.getString(c.getColumnIndex(Database.CART_orders.Order_image));
//                String quantity = c.getString(c.getColumnIndex(Database.CART_orders.Order_quanity));
//                String sellername = c.getString(c.getColumnIndex(Database.CART_orders.Order_sellername));
//                String prize = c.getString(c.getColumnIndex(Database.CART_orders.Order_prize));
//                String qcount = c.getString(c.getColumnIndex(Database.CART_orders.Order_qCount));
//                String rID = c.getString(c.getColumnIndex(Database.CART_orders.ID));
//                String authorID = c.getString(c.getColumnIndex(Database.CART_orders.Order_AuthorID));
//
//                Bean bean = new Bean();
//                bean.setCartID(product_id);
//                bean.setCartName(name);
//                bean.setCartImage(image);
//                bean.setCartquatity(quantity);
//                bean.setCartSellername(sellername);
//                bean.setCartPrize(prize);
//                bean.setSp_count(qcount);
//                bean.setRow_id(rID);
//                bean.setAuthorId(authorID);
//
//                list.add(bean);
//                Log.d("--sql output", "client_id: " + product_id);
//                c.moveToNext();
//            }
//            c.close();
//        } catch (Exception e) {
//            Log.d("--sql error", "while fetching: " + e);
//
//        }
//        return list;
//    }


//	public boolean Insert_alarm_schedule(String name, String description,
//			String time, String type, String status) {
//		long id = 0;
//		// TODO Auto-generated method stub
//		if (!isExist_schedule(time)) {
//			try {
//				ContentValues values = new ContentValues();
//				values.put(Database.ALARM_COLUMN_Title, name);
//				values.put(Database.ALARM_COLUMN_Description, description);
//				values.put(Database.ALARM_COLUMN_Time, time);
//				values.put(Database.ALARM_COLUMN_Type, type);
//				values.put(Database.ALARM_COLUMN_Status, status);
//				id = sqlite.insert(Database.ALARM_TABLE_NAME, null, values);
//			} catch (Exception e) {
//				Log.d("--sql error", "while insert in database: " + e);
//			}
//			if (id > 0) {
//				return true;
//			}
//		}
//		return false;
//	}
//

}
