package matiarmanush.com.matiarapp.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Database extends SQLiteOpenHelper {

    public static final int VERSION = 1;
    public static final String DB_NAME = "trip.db";


    public class Table {
        public static final String CartItem = "_trips";
    }

    public class Column {


        public class Cart {
            private static final String RowId = "_rid";
            private static final String ProductId = "_pid";
            private static final String CategoryId = "_cat";
            private static final String ParentId = "_pid";
            private static final String Name = "_name";
            private static final String Image = "_image";
            private static final String Quantity = "_quant";
            private static final String Count = "_class";
            private static final String isAdded = "_isadded";
            private static final String price = "_price";

        }
    }


    public Database(Context context) {
        super(context, DB_NAME, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTableCart = "create table " + Table.CartItem + " (" + Column.Cart.RowId + " integer primary key, "
                + Column.Cart.CategoryId + " text, "
                + Column.Cart.Count + " text, "
                + Column.Cart.Image + " text, "
                + Column.Cart.isAdded + " text, "
                + Column.Cart.Name + " text, "
                + Column.Cart.ParentId + " text, "
                + Column.Cart.price + " text, "
                + Column.Cart.ProductId + " text, "
                + Column.Cart.Quantity + " text)";



        db.execSQL(createTableCart);

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }


}
