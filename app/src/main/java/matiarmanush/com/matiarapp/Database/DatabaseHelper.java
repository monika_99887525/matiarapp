package matiarmanush.com.matiarapp.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import matiarmanush.com.matiarapp.Models.ProductModel;

import static matiarmanush.com.matiarapp.Database.DatabaseHelper.Column.Cart.price;


public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;
    public static final String DB_NAME = "mdb1.db";
    private static DatabaseHelper databaseHelper = null;
    private static SQLiteDatabase sqlite;


    public class Table {
        public static final String CartItem = "mydb";
    }

    public class Column {


        public class Cart {
            private static final String RowId = "_rid";
            public static final String ProductId = "_proid";
            public static final String CategoryId = "_cat";
            public static final String ParentId = "_pid";
            public static final String Name = "_name";
            public static final String Image = "_image";
            public static final String Quantity = "_quant";
            public static final String Count = "_class";
            public static final String isAdded = "_isadded";
            public static final String price = "_price";
            public static final String newPrice = "_newprice";

        }
    }


    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);

    }

    public static void initialize(Context context) {
        createInstance(context);
    }

    private static void createInstance(Context context) {
        databaseHelper = new DatabaseHelper(context);
        sqlite = databaseHelper.getWritableDatabase();
    }

    public static DatabaseHelper getInstance() {
        return databaseHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTableCart = "create table " + Table.CartItem + " (" + Column.Cart.RowId + " integer primary key, "
                + Column.Cart.ProductId + " text, "
                + Column.Cart.CategoryId + " text, "
                + Column.Cart.ParentId + " text, "
                + Column.Cart.Name + " text, "
                + Column.Cart.Image + " text, "
                + Column.Cart.Quantity + " text, "
                + Column.Cart.Count + " text, "
                + Column.Cart.isAdded + " text, "
                + price + " text, "
                + Column.Cart.newPrice + " text)";

        db.execSQL(createTableCart);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /**
     * @param ProductId  if exist the user update method to update count value else insert new product in cart
     * @param CategoryId
     * @param ParentId
     * @param Name
     * @param Image
     * @param Count
     * @param isAdded
     * @param price
     * @return
     */
    public boolean insertCartItem(String ProductId, String CategoryId, String ParentId, String Name, String Image, String Count, String isAdded, String price, String newprice) {
        ContentValues values = new ContentValues();

        values.put(Column.Cart.ProductId, ProductId);
        values.put(Column.Cart.CategoryId, CategoryId);
        values.put(Column.Cart.ParentId, ParentId);
        values.put(Column.Cart.Name, Name);
        values.put(Column.Cart.Image, Image);
        values.put(Column.Cart.Count, Count);
        values.put(Column.Cart.isAdded, isAdded);
        values.put(price, price);
        values.put(Column.Cart.newPrice, newprice);

        long id = 0;
        try {
            id = sqlite.insert(DatabaseHelper.Table.CartItem, null, values);
        } catch (Exception e) {
            Log.d("--sql error", "while insert in database: " + e);
            e.printStackTrace();
            return false;
        }
        if (id > 0) {
            return true;
        }

        return false;
    }

    private ProductModel resolveCursorCart(Cursor cursor) {
        String productid = cursor.getString(cursor.getColumnIndex(Column.Cart.ProductId));
        String catid = cursor.getString(cursor.getColumnIndex(Column.Cart.CategoryId));
        String parenyid = cursor.getString(cursor.getColumnIndex(Column.Cart.ParentId));
        String name = cursor.getString(cursor.getColumnIndex(Column.Cart.Name));
        String image = cursor.getString(cursor.getColumnIndex(Column.Cart.Image));
        String quantity = cursor.getString(cursor.getColumnIndex(Column.Cart.Quantity));
        String count = cursor.getString(cursor.getColumnIndex(Column.Cart.Count));
        String isadded = cursor.getString(cursor.getColumnIndex(Column.Cart.isAdded));
        String price = cursor.getString(cursor.getColumnIndex(Column.Cart.price));
        String newprice = cursor.getString(cursor.getColumnIndex(Column.Cart.newPrice));

        ProductModel categoryModel = new ProductModel();
        categoryModel.setProductId(productid);
        categoryModel.setCategoryId(catid);
        categoryModel.setParentId(parenyid);
        categoryModel.setName(name);
        categoryModel.setImage(image);
        categoryModel.setQuantity(quantity);
        categoryModel.setCount(count);
        categoryModel.setIsAdded(isadded);
        categoryModel.setPrice(price);
        categoryModel.setNewPrice(newprice);

        return categoryModel;
    }

    /**
     * get cart all items to show in my cart list and calculate total amount
     *
     * @return
     */
    public ArrayList<ProductModel> getCartItems_countPrice() {
        ArrayList<ProductModel> categoryModels = new ArrayList<>();
        try {
            Cursor cursor = sqlite.query(Table.CartItem, new String[]{Column.Cart.Count, Column.Cart.newPrice}, null, null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ProductModel categoryModel = new ProductModel();
                String count = cursor.getString(cursor.getColumnIndex(Column.Cart.Count));
                String newprice = cursor.getString(cursor.getColumnIndex(Column.Cart.newPrice));

                categoryModel.setCount(count);
              categoryModel.setNewPrice(newprice);
                categoryModels.add(categoryModel);

                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categoryModels;
    }

    public ArrayList<ProductModel> getCartItems() {
        ArrayList<ProductModel> categoryModels = new ArrayList<>();
        try {
            Cursor cursor = sqlite.query(Table.CartItem, getCartColumn(), null, null, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                ProductModel model = resolveCursorCart(cursor);
                categoryModels.add(model);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return categoryModels;
    }

    /**
     * check if product exist to filter product list on different pages..
     *
     * @param productId if exist then pick the model from this and add to bean list while showing in adapter else add webservice details to bean.
     * @return
     */

    public ProductModel getProductDetails(String productId) {
        ProductModel model = null;
        try {
            Cursor cursor = sqlite.query(Table.CartItem, getCartColumn(), Column.Cart.ProductId + "=?", new String[]{String.valueOf(productId)}, null, null, null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                model = resolveCursorCart(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;
    }
    /*
    checkif product exist in cart while inserting ...if exist the update count vale if not then insert in table
     */

    public boolean isExistProductId(String productId) {
        try {
            Cursor cursor = sqlite.query(Table.CartItem, getCartColumn(), Column.Cart.ProductId + "=?", new String[]{String.valueOf(productId)}, null, null, null);
            boolean b = cursor.moveToFirst();
            cursor.close();
            return b;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;

    }


    private String[] getCartColumn() {
        return new String[]{
                Column.Cart.RowId,
                Column.Cart.ProductId,
                Column.Cart.CategoryId,
                Column.Cart.ParentId,
                Column.Cart.Name,
                Column.Cart.Image,
                Column.Cart.Quantity,
                Column.Cart.Count,
                Column.Cart.isAdded,
                price,
                Column.Cart.newPrice,

        };
    }

    /**
     * when cart item id less than 1 then delete that product from cart
     *
     * @param productID
     * @return
     */

    public boolean deleteItemFromCart(String productID) {

        int count;
        try {

            count = sqlite.delete(Table.CartItem, Column.Cart.ProductId + "=?", new String[]{String.valueOf(productID)});

        } catch (Exception e) {
            Log.d("--sql error", "while insert in database: " + e);
            e.printStackTrace();
            return false;
        }
        if (count > 0) {
            return true;
        }

        return false;

    }

    /**
     * update count when user press plus button for product
     *
     * @param productId
     * @param count
     * @return
     */
    public boolean updateCart(String productId, String count) {
        ContentValues values = new ContentValues();
        values.put(Column.Cart.Count, count);

        long id;
        try {

            id = sqlite.update(Table.CartItem, values, Column.Cart.ProductId + "=?", new String[]{productId});

        } catch (Exception e) {
            Log.d("--sql error", "while insert in database: " + e);
            e.printStackTrace();
            return false;
        }
        if (id > 0) {
            return true;
        }

        return false;
    }


//    @Nullable
//    public StudentModel getStudentFromTrip(String nfcId, long tripId) {
//        StudentModel model = null;
//        try {
//            Cursor cursor = sqLite.query(Table.TripStudents, getTripStudentColumn(), Column.TripStudent.TripId + "=? and " + Column.TripStudent.NfcId + "=?", new String[]{String.valueOf(tripId), nfcId}, null, null, null);
//            if (cursor.moveToFirst())
//                model = resolveCursorTripStudents(cursor);
//            cursor.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return model;
//    }


//    @Nullable
//    public StudentModel getStudent(String nfcId, long tripId) {
//        StudentModel model = null;
//        try {
//            Cursor cursor = sqLite.query(Table.TripStudents, getTripStudentColumn(), Column.TripStudent.TripId + "=? and " + Column.TripStudent.NfcId + "=?", new String[]{String.valueOf(tripId), nfcId}, null, null, null);
//            if (cursor.moveToFirst()) {
//                model = resolveCursorTripStudents(cursor);
//
//            }
//
//            cursor.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return model;
//    }


}
