package matiarmanush.com.matiarapp.classes;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;

import java.util.ArrayList;

import matiarmanush.com.matiarapp.Models.ProductModel;
import matiarmanush.com.matiarapp.R;
import matiarmanush.com.matiarapp.databinding.ActivitySplashBinding;
import matiarmanush.com.matiarapp.generic.BaseActivity;


public class Splash extends BaseActivity {
    ActivitySplashBinding splash;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splash = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        ArrayList<ProductModel> list = databaseHelper.getCartItems_countPrice();
        int total = 0;
//        for (int i = 0; i < list.size(); i++) {
//            int value = Integer.parseInt(list.get(i).getCount());
//            int price = Integer.parseInt(list.get(i).getNewPrice());
//            int oneprice = value * price;
//            total=total+oneprice;
//        }

        getSupportActionBar().hide();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                Intent i = new Intent(Splash.this, Home.class);
//                startActivity(i);
//                finish();

            }
        }, SPLASH_TIME_OUT);
    }


}