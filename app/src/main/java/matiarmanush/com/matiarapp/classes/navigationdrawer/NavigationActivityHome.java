package matiarmanush.com.matiarapp.classes.navigationdrawer;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import matiarmanush.com.matiarapp.Fragments.MyAccountFragment;
import matiarmanush.com.matiarapp.R;
import matiarmanush.com.matiarapp.generic.HandleToolbar;

public class NavigationActivityHome extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
public static final String TAG="navigation";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_naviagtion_home);
        final NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        HandleToolbar toolbar= new HandleToolbar(NavigationActivityHome.this );
        toolbar.showNAvIcon();
        toolbar.showCartIcon();
        toolbar.showNotificationIcon();
        toolbar.showSearchIcon();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(NavigationActivityHome.this);

        toolbar.setOnNavigationClickListener(new HandleToolbar.OnNavigationClickListener() {
            @Override
            public void onClick() {
                Log.d(TAG, "onClick:vfvdd ");
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.naviagtion_activity_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_acct) {
            MyAccountFragment myAccountFragment = new MyAccountFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            transaction.replace(R.id.frag,myAccountFragment).addToBackStack(null).commit();
            // Handle the camera action
        } else if (id == R.id.nav_cart) {


        } else if (id == R.id.nav_coupons) {

        } else if (id == R.id.nav_offers) {

        }
        else if (id == R.id.nav_free_shopping) {

        }
        else if (id == R.id.nav_manage_address) {

        }

        else if (id == R.id.nav_help) {

        }
        else if (id == R.id.nav_contact_us) {

        }
        else if (id == R.id.nav_share) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
