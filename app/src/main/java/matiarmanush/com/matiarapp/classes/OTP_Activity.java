package matiarmanush.com.matiarapp.classes;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import matiarmanush.com.matiarapp.R;
import matiarmanush.com.matiarapp.databinding.ActivityOtpBinding;
import matiarmanush.com.matiarapp.generic.BaseActivity;

/**
 * Created by Lakhwinder on 11/7/2017.
 */

public class OTP_Activity extends BaseActivity {
ActivityOtpBinding bindingg;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindingg = DataBindingUtil.setContentView(this, R.layout.activity_otp);
        bindingg.setOpt(OTP_Activity.this);


    }
    public void verifyClick(){
        if (!validateOTP()) {
            return;
        }
        Toast.makeText(OTP_Activity.this, "Verified", Toast.LENGTH_SHORT).show();
    }

    private boolean validateOTP() {
        if (bindingg.inputOtp.getText().toString().trim().isEmpty()) {
            bindingg.inputLayoutOpt.setError(getString(R.string.nullerror));
            requestFocus(bindingg.inputOtp);
            return false;
        } else {
            bindingg.inputLayoutOpt.setErrorEnabled(false);
        }

        return true;
    }
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_otp:
                    validateOTP();
                    break;

            }
        }
    }

}
